# SamKnows BE Test

## How to run 
This is a symfony minimal application, running inside a docker container with php 7.4 (cli). 
You can test this application by spinning the docker container and then running the command itself.
In order to follow these instructions you must clone this repository onto your local machine and have `docker` and `docker-compose` installed.
To spin the container, in your terminal session, go to the directory where you cloned this app and run: 

`docker-compose up .`

To run the command, you must use the same instructions as any symfony command line application. 

In this case, the command is simply called `process`. So the command we should be calling should be `./bin/console app:process`. 
If you run the command on its own, it will require 2 parameters. One for the input folder and another for the output folder. 
Since this app is based on the examples given initially, there are 2 json files inside `public/input` folder which can be used for demo purposes. 
There is also a folder at  `public/output` which should be empty with exeption of 2 backup files for comaparison. 

The full correct command should then be: 

`./bin/console app:process public/input/ public/output/`

This should produce 2 files in the output folder named `1.txt` and `2.txt` with a report coming from the command logic. 

The naming of the files is based off the input files, so feel free to create your own json files and throw them into the input. 
Since there is no validation on the files or its contents, if they are not of the same type as the examples, the app should crash and throw an error. 

## How to run tests 
If everything was successful in the previous step, you should have a docker container running. 

With that, you can simply connect to a shell inside that container, go to the app main folder and run phpunit tests.

To run attach the shell, run: 

`docker exec -it <container-id> bash`

To run the tests, run: 

`cd /APP` and then `./bin/phpunit --testdox`

## TODO - Outstanding
For this app I pretty much only created the happy path of all the tests, leaving incosnsitencies, negative values, typos and all sorts of other possibilities, to be done.
Additionally, I may have left a bit of polish out of the app, mostly because I ran out of time, but given this was on top of a cli command in symfony, many OOP best practices are somewhat hindered, namely `single responsibility` and `lack of cohesion` both on the tests but especially on the source code. 

