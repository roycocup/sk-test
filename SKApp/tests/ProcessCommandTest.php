<?php

namespace App\Tests;

use DateInterval;
use DateTime;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

use function PHPUnit\Framework\directoryExists;

class ProcessCommandTest extends KernelTestCase
{
    protected function setUp(): void
    {
        $kernel = static::createKernel();
        $this->application = new Application($kernel);

        $this->command = $this->application->find('app:process');
        $this->commandTester = new CommandTester($this->command);
        $this->inFolder = 'public/intests/';
        $this->outFolder = 'public/outtests/';

        $this->params = ['if' => $this->inFolder, 'of' => $this->outFolder];

        @mkdir($this->inFolder);
        @mkdir($this->outFolder);
    }


    protected function tearDown(): void
    {
        if (is_dir($this->inFolder)) {
            exec('rm -rf ' . $this->inFolder);
        }

        if (is_dir($this->outFolder)) {
            exec('rm -rf ' . $this->outFolder);
        }
    }

    protected function createInFile($filename, $data = null)
    {
        if (!$data) {
            $data = [];

            $curDate = (new DateTime('now'))->modify('-1 month');
            $now = new DateTime('now');

            while (0 != $curDate->diff($now)->days) {
                $curDate->modify('+1 day');
                $bits = random_int(1111111111, 9999999999) / 10;
                $data[] = ["metricValue" => $bits, "dtime" => $curDate->format('Y-m-d')];
            }
        }

        file_put_contents($this->inFolder . $filename, json_encode($data));
    }


    /**
     * @group integration
     */
    public function test_can_execute_command(): void
    {
        $this->commandTester->execute($this->params);

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString('Process Started', $output);
    }

    /**
     * @group integration
     */
    public function test_creates_output_file_based_on_input_file(): void
    {
        $this->createInFile("doesThisExist.txt");
        $this->commandTester->execute($this->params);

        $this->assertTrue(file_exists($this->outFolder . 'doesThisExist.txt'));
    }

    /**
     * @group integration
     */
    public function test_can_read_and_create_file_without_trailing_slash(): void
    {
        $this->createInFile("doesThisExist.txt");

        $inFolder = 'public/intests';
        $outFolder = 'public/outtests';
        $params = ['if' => $inFolder, 'of' => $outFolder];

        $this->commandTester->execute($params);

        $this->assertTrue(file_exists($this->outFolder . 'doesThisExist.txt'));
    }

    /**
     * @group integration
     */
    public function test_can_write_file_with_different_extension(): void
    {
        $this->createInFile("doesThisExist.json");
        $this->commandTester->execute($this->params);

        $this->assertTrue(file_exists($this->outFolder . 'doesThisExist.txt'));
    }

    /**
     * @group integration
     */
    public function test_given_an_input_file_output_will_contain_the_following(): void
    {
        $today = (new \DateTime('now'));
        $yesterday = (new \DateTime('now'))->modify('-2 day');
        $data[] = ["metricValue" => 11111111, "dtime" => $yesterday->format('Y-m-d')];
        $data[] = ["metricValue" => 11111111, "dtime" => $today->format('Y-m-d')];
        $this->createInFile("basic.json", $data);
        $this->commandTester->execute($this->params);

        $this->assertTrue(file_exists($this->outFolder . 'basic.txt'));
        $actual = file_get_contents($this->outFolder . 'basic.txt');

        $expected = <<<EOT

        SamKnows Metric Analyser v1.0.0
        ===============================
        Period checked:

            From: {$yesterday->format('Y-m-d')}
            To:   {$today->format('Y-m-d')}

        Statistics:

            Unit: Megabits per second

            Average: 11.11
            Min: 11.11
            Max: 11.11
            Median: 11.11
        EOT;

        $this->assertSame($expected, $actual);
    }

    /**
     * @group integration
     */
    public function test_given_input_file_with_underperforming_data_we_should_see_the_following(): void
    {
        $today = (new \DateTime('now'));
        $data[] = ["metricValue" => pow(10, 8), "dtime" => (new \DateTime('now'))->modify('-5 day')->format('Y-m-d')];
        $data[] = ["metricValue" => pow(10, 8), "dtime" => (new \DateTime('now'))->modify('-4 day')->format('Y-m-d')];
        $data[] = ["metricValue" => pow(10, 7), "dtime" => (new \DateTime('now'))->modify('-3 day')->format('Y-m-d')];
        $data[] = ["metricValue" => pow(10, 7), "dtime" => (new \DateTime('now'))->modify('-2 day')->format('Y-m-d')];
        $data[] = ["metricValue" => pow(10, 8), "dtime" => (new \DateTime('now'))->modify('-1 day')->format('Y-m-d')];
        $data[] = ["metricValue" => pow(10, 8), "dtime" => $today->format('Y-m-d')];
        $this->createInFile("underperformance.json", $data);
        $this->commandTester->execute($this->params);

        $actual = file_get_contents($this->outFolder . 'underperformance.txt');
        
        $this->assertNotEquals(false, strpos($actual, 'Under-performing'), "Underperforming test was not found in report"); 
        
        $underDate1 = (new \DateTime('now'))->modify('-3 day')->format('Y-m-d');
        $underDate2 = (new \DateTime('now'))->modify('-2 day')->format('Y-m-d');
        $okDate = (new \DateTime('now'))->modify('-4 day')->format('Y-m-d');
        $this->assertNotEquals(false, strpos($actual, $underDate1), "Underperforming test was not found in report"); 
        $this->assertNotEquals(false, strpos($actual, $underDate2), "Underperforming test was not found in report"); 
        $this->assertFalse(strpos($actual, $okDate), "Underperforming test was not found in report"); 
    }
}
