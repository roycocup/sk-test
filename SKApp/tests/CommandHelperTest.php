<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Command\ProcessCommand;
use Exception;

class CommandHelperTest extends TestCase
{
    public function setUp(): void
    {
        $this->sut = new ProcessCommand('someName');
    }


    public function test_can_extract_date_details_from_unordered_array(): void
    {
        $today = new \DateTime('now');
        $startDate = (new \DateTime('now'))->modify('-10 days');
        $curDate = clone($startDate);

        // prepare the array with ordered data
        $data = [];
        // adding the first day to the array as its harder to check for equality after
        $data[] = ["metricValue" => '11111', "dtime" => $startDate->format('Y-m-d')];
        while (0 != $curDate->diff($today)->days) {
            $curDate->modify('+1 day');
            $data[] = ["metricValue" => '11111', "dtime" => $curDate->format('Y-m-d')];
        }

        // randomise the array
        shuffle($data);

        $dateDetails = $this->sut->getDateDetails($data);
        $this->assertEquals($startDate->format('Y-m-d'), $dateDetails['earliest']);
        $this->assertEquals($today->format('Y-m-d'), $dateDetails['latest']);
    }


    public function test_can_get_statistical_details_from_array(): void 
    {
        $data = [];
        $data[] = ["metricValue" => 11111111, "dtime" => 'any date'];
        $data[] = ["metricValue" => 11111111, "dtime" => 'any date'];
        $data[] = ["metricValue" => 11111111, "dtime" => 'any date'];

        $stats = $this->sut->getStatistics($data);
        $this->assertEquals(11.11, $stats['max']);
        $this->assertEquals(11.11, $stats['min']);
        $this->assertEquals(11.11, $stats['avg']);
        $this->assertEquals(11.11, $stats['med']);

    }

    public function test_can_get_accurate_statistical_results_from_unordered_array(): void
    {
        $data = [];
        $data[] = ["metricValue" => 11111111, "dtime" => 'any date'];
        $data[] = ["metricValue" => 99991111, "dtime" => 'any date'];
        $data[] = ["metricValue" => 33331111, "dtime" => 'any date'];
        

        $stats = $this->sut->getStatistics($data);
        $this->assertEquals(99.99, $stats['max']);
        $this->assertEquals(11.11, $stats['min']);
        $this->assertEquals(48.14, $stats['avg']);
        $this->assertEquals(33.33, $stats['med']);
    }

    public function test_can_calculate_the_mean_of_array(): void
    {
        $data = [1,2,3,4,5];
        $mean = $this->sut->calcMean($data);
        $this->assertEquals(3, $mean);

        $data = [1,1,1,1,1,3];
        $mean = $this->sut->calcMean($data);
        $this->assertEquals(1.33, round($mean, 2));
    }

    public function test_can_get_median_of_array(): void
    {
        $data = [1,2,3,4,5];
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(3, $median);

        $data = [1,2,3,4,5];
        shuffle($data);
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(3, $median);

        $data = [1,2,3,4,5,6];
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(4, $median);

        $data = [1,1,2,2,3];
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(2, $median);

        $data = [1,1];
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(1, $median);

        $data = [1,1,1];
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(1, $median);

        $data = [100,200,300];
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(200, $median);

        $data = [100,200,200,300,305,200];
        $median = $this->sut->calcMedian($data);
        $this->assertEquals(200, $median);
    }

    public function test_can_identify_underperformance_in_array(): void
    {
        $data = [];
        $data[] = ["metricValue" => 100, "dtime" => '2021-01-01'];
        $data[] = ["metricValue" => 100, "dtime" => '2021-01-02'];
        $data[] = ["metricValue" => 20, "dtime" => '2021-01-03'];
        $data[] = ["metricValue" => 10, "dtime" => '2021-01-04'];
        $data[] = ["metricValue" => 200, "dtime" => '2021-01-05'];

        $actual = $this->sut->getUnderperformingDates($data);
        $expected = ['dates' => ['2021-01-03', '2021-01-04',]];
        $this->assertEquals($expected, $actual); 
    }
}
