<?php

namespace App\Command;

use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ProcessCommand extends Command
{

    protected static $defaultName = 'app:process';

    protected function configure(): void
    {
        $this
            ->addArgument('if', InputArgument::REQUIRED, 'Input folder - The relative path to the input folder')
            ->addArgument('of', InputArgument::REQUIRED, 'Output folder - The relative path to the output folder');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Process Started...');

        $APP_ROOT = dirname(dirname(__DIR__)) . "/";

        $inputFolder = $APP_ROOT . $input->getArgument('if');
        $this->includeTrailingSlash($inputFolder);
        $inputFiles = glob($inputFolder . '*');


        foreach ($inputFiles as $inputFile) {
            $filename = pathinfo($inputFile)['filename'];
            $outFolder = $APP_ROOT . $input->getArgument('of');
            $this->includeTrailingSlash($outFolder);
            $resultStr = $this->processInputFile($inputFile);
            file_put_contents($outFolder . $filename . '.txt', $resultStr);
        }

        $output->writeln('Process Ended');

        return Command::SUCCESS;
    }

    private function includeTrailingSlash(string &$subject): void
    {
        if (substr($subject, -1, 1) != '/') {
            $subject = $subject . '/';
        }
    }

    private function processInputFile(string $inputFile): string
    {
        $dataArray = json_decode(file_get_contents($inputFile), true);
        $dateDetails = $this->getDateDetails($dataArray);
        $statistics = $this->getStatistics($dataArray);
        $underperformingDates = $this->getUnderperformingDates($dataArray);
        
        $from = $dateDetails['earliest'];
        $to = $dateDetails['latest'];
        
        $avg = $statistics['avg'];
        $min = $statistics['min'];
        $max = $statistics['max'];
        $med = $statistics['med'];

        $returnStr = <<<EOT

        SamKnows Metric Analyser v1.0.0
        ===============================
        Period checked:

            From: {$from}
            To:   {$to}

        Statistics:

            Unit: Megabits per second

            Average: {$avg}
            Min: {$min}
            Max: {$max}
            Median: {$med}
        EOT;

        $underStr = '';
        if (!empty($underperformingDates['dates'])) {
            $firstDate = $underperformingDates['dates'][0];
            $lastDate = end($underperformingDates['dates']); 
            $underStr = <<<EOT
                \n
            Under-performing periods:

                * The period between {$firstDate} and {$lastDate}
                was under-performing.
            EOT;
        }

        return $returnStr . $underStr;
    }

    public function getDateDetails(array $data): array
    {
        $times = [];
        foreach ($data as $entry) {
            $times[] = strtotime($entry['dtime']);
        }

        sort($times);
        $earliest = (new DateTime())->setTimestamp($times[0]);
        $latest = (new DateTime())->setTimestamp($times[count($times) - 1]);

        return ['earliest' => $earliest->format('Y-m-d'), 'latest' => $latest->format('Y-m-d')];
    }

    public function getStatistics(array $data): array
    {
        $cleanArray = [];
        foreach($data as $item)
        {
            $cleanArray[] = $item['metricValue'];
        }

        sort($cleanArray);

        $resp = [];
        $bitsToMbits = pow(10, 6);
        
        $resp['max'] = round((end($cleanArray) / $bitsToMbits), 2);
        $resp['min'] = round((reset($cleanArray)/$bitsToMbits), 2);
        $resp['avg'] = round(($this->calcMean($cleanArray)/$bitsToMbits), 2);
        $resp['med'] = round(($this->calcMedian($cleanArray)/$bitsToMbits), 2);
        return $resp;
    }

    public function calcMean(array $data)
    {
        return array_sum($data)/count($data); 
    }

    public function calcMedian(array $data)
    {
        $count = count($data);
        sort($data);
        
        if ($count%2==1) {
            return $data[$count/2];
        } else {
            $index = ($count + 1) / 2;
            return $data[$index];
        }
        
    }

    public function getUnderperformingDates(array $data): array
    {
        // setting the threshold at x% below avg
        $threshold = .10;
        foreach($data as $item){
            $cleanArray[] = $item['metricValue'];
        }

        $mean = $this->calcMean($cleanArray);
        
        $limit = $mean - ($threshold * $mean);
        
        $underPerformingDates = [];
        foreach($data as $item){
            if ($item['metricValue'] <= $limit) {
                $underPerformingDates[] = $item['dtime'];
            }
        }

        return ['dates' => $underPerformingDates]; 
    }
}
